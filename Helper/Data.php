<?php
namespace Magebees\Ajaxquickview\Helper;
/**
 * Class Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
   
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,       
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {        
        $this->productMetadata = $productMetadata;        
        parent::__construct($context);
    }
    public function getMagentoVersion()
    {
        return $this->productMetadata->getVersion();
    }
     
}
